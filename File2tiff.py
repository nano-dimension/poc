import subprocess
import os ,shutil
import pyautogui as msg
import tkinter , tkfilebrowser
from time import sleep
import re
import csv
import json
from PIL import Image, ImageOps ,ImageMath ,ImageChops
import zipfile
import gc
import cv2
import numpy as np
root = tkinter.Tk()

class layer():
    
    def __init__(self,Number,Name,Type,Material,Thickness,Weight,Dk,Orientation,file=''):
        self.Number = Number
        self.Name = Name
        self.Type = Type
        self.Material = Material
        self.Thickness = Thickness
        self.Weight = Weight
        self.Dk = Dk
        self.Orientation = Orientation



def main():
    clear()
    matching_drill = []
    diamter_del = 0.0
    matching_layer = []
    matching_grb = []
    files = tkfilebrowser.askopenfilenames(root,initialdir='C:/WCAD/GBRIP64/EXAMPLES')
    matching_drill = [file for file in files if ".tx" in file.lower()]
    matching_layer = [file for file in files if ".csv" in file.lower()]
    matching_grb = [file for file in files if ".g" in file.lower() and not'.gm' in file.lower()]
    optimized = False
    ldp = [file for file in files if ".ldp" in file.lower()]
    diamter_del = float(msg.prompt("please enter miniumum diamter",default='0.0'))
    org_grb = matching_grb[:]
    ext_layers = [file.split('.')[-1].lower() for file in matching_grb if ".g" in file.lower() and not'.gm' in file.lower()]
    if org_grb != []:
        name_proj = os.path.basename(org_grb[0]).split('.')[0]
    for drill in matching_drill :
        drl2grb_all(drill,matching_grb ,diamter_del,optimized)
    temp_gerb = route2all(matching_grb,files)

    tiffs = rastrize_list_invert(temp_gerb)
    result,drl,drl_insu,existing_layers = order(ldp,org_grb,tiffs)
    pack = [file for file in tiffs if ".g" in file and not '.drl' in file] 
    combine2prepreg(result,drl,drl_insu,pack,existing_layers)
    layer_thick(matching_layer[0],ext_layers)
    zip_directory('./Output/tiffs/pack/cond/', './'+name_proj+'.pcbjc')


def clear():
    folder = './Output/'
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))
    sleep(2)
    os.mkdir( './Output/tiffs/' );
    os.mkdir( './Output/work/' );
    os.mkdir( './Output/Gerber_w_grb/' );
    os.mkdir( './Output/tiffs/pack/' );
    os.mkdir( './Output/tiffs/pack/cond/' );

def layer_thick(matching_layer,ext_layers):
    save_path = './Output/tiffs/pack/cond/'
    lines = []
    csvfile = open(matching_layer, newline='') 
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        if row !=[] and len(row)>=7 and (row[0] == '' or row[6] != ''):
            if row[1] == 'Top Overlay':
                if row[6] == '':
                    lines.append([row[1],'0.050','gto'])
                else:
                    lines.append([row[1],row[6],'gto'])
            elif row[1] == 'Top Solder':
                lines.append([row[1],row[6],'gts'])
            elif row[1] == 'Top Layer':
                lines.append([row[1],row[6],'gtl'])
            elif row[1] == 'Bottom Layer':
                lines.append([row[1],row[6],'gbl'])
            elif row[1] == 'Bottom Solder':
                lines.append([row[1],row[6],'gbs'])
            elif row[1] == 'Bottom Overlay':
                if row[6] == '':
                    lines.append([row[1],'0.050','gbo'])
                else:
                    lines.append([row[1],row[6],'gbo'])
            elif row[1] == 'Name':
                pass
            else:
                try:
                    kind,num = row[1].split(' ')
                    lines.append([kind,row[6],'g'+num])
                except:
                    pass
    
    Separations = []
    for line in lines :
        if line[2] in ext_layers:
            if line[0] != 'Dielectric':
                Separations.append({
         "SeparatingMaterials" : True,
         "Separations" :
                    [
                    {
                    "File" : 'cond.'+line[2]+".tif",
                    "LayerThicknessUM" : int(float(line[1])*1000),
                    "Separation" : "Conductor"
                    },
                    {
                    "File" : 'insu.'+line[2]+".tif" ,
                    "LayerThicknessUM" : int(float(line[1])*1000),
                    "Separation" : "Insulator"
                    }
                ]}
                )
            if line[0] == 'Dielectric' or line[2] == 'gto' or line[2] == 'gtl' or line[2] == 'gts' or line[2] == 'gbo' or line[2] == 'gbl' or line[2] == 'gbs':
                Separations.append({
         "SeparatingMaterials" : True,
         "Separations" :
                    [
                    {
                    "File" : 'cond.prepreg.'+line[2]+".tif",
                    "LayerThicknessUM" : int(float(line[1])*1000),
                    "Separation" : "Conductor"
                    },
                    {
                    "File" : 'insu.prepreg.'+line[2]+".tif" ,
                    "LayerThicknessUM" : int(float(line[1])*1000),
                    "Separation" : "Insulator"
                    }
                ]}
                )
    json_data ={"Document" : "PCBJetJob",
             "Layers" :  Separations,
                        "Name" : "LGA",
                        "PositionInMM" : {
                        "GroupAxis" : 0,
                        "PrintAxis" : 0},
                "Recipe" : "TO Standard",
                "Separations" : [
                    {"ResolutionInUM" : {
                            "GroupAxis" : 35.25,
                            "PrintAxis" : 36},
                    "Separation" : "Conductor"},
                    {"ResolutionInUM" : {
                            "GroupAxis" : 35.25,
                            "PrintAxis" : 36}
                    ,"Separation" : "Insulator"}]
                    ,"Version" : "1.0"}
    with open(save_path+'pcbj.info', 'w') as outfile:
        outfile.write(json.dumps(json_data, indent=2))

def combine2prepreg(result,drl,drl_insu,pack,existing_layers):
    cond = []
    insu = []
    save_path = './Output/tiffs/pack/cond/'
    i = 0
    for tiff in pack :
        layert,kind = tiff.split('.')[-5],tiff.split('.')[-2]
        for prereg in result:
            lower,upper = prereg.split('.')[-2],prereg.split('.')[-3]
            if lower == layert or upper == layert :
                prereg_im = Image.open(prereg).convert('1')
                tiff_im = Image.open(tiff).convert('1')
                PIL = ImageChops.add( tiff_im,prereg_im,scale=1.0,offset=0)
                prereg_im_inver =  ImageMath.eval('255-(a)',a=prereg_im).convert('1')
                PIL_inver =  ImageMath.eval('255-(a)',a=tiff_im).convert('1')
                for drill in drl:
                    lower_d,upper_d = drill.split('.')[-2],drill.split('.')[-3]
                    if lower_d == lower and lower == layert or upper == layert and upper_d == layert :

                        drl_im= Image.open(drill).convert('1')
                        PIL = ImageChops.subtract( PIL,drl_im,scale=1.0,offset=0)
                        prereg_im = ImageChops.subtract( prereg_im,drl_im,scale=1.0,offset=0)

                        for drill_insu in drl_insu:
                            lower_di,upper_di = drill.split('.')[-2],drill.split('.')[-3]
                            if lower_di == lower and lower == layert or upper == layert and upper_di == layert :
                                drill_insu_im = Image.open(drill_insu).convert('1')
                                prereg_im_inver = ImageChops.subtract( prereg_im_inver,drill_insu_im,scale=1.0,offset=0)
                                PIL_inver = ImageChops.subtract( PIL_inver,drill_insu_im,scale=1.0,offset=0)

                
                prereg_im.save(save_path+'cond.prepeg.'+layert+'.tif')
                PIL.save(save_path+'cond.'+layert+'.tif')

                cond.append(save_path+'cond.'+layert+'.tif')
                cond.append(save_path+'cond.prepeg.'+layert+'.tif')

                prereg_im_inver.save(save_path+'insu.prepeg.'+layert+'.tif')
                PIL_inver.save(save_path+'insu.'+layert+'.tif')

                insu.append(save_path+'insu.'+layert+'.tif')
                insu.append(save_path+'insu.prepeg.'+layert+'.tif')


            i = i+1

    if existing_layers[0] == ['gts','gtl']  : 
        img1 = cv2.imread(save_path+'insu.gtl.tif')
        img2 = cv2.imread(save_path+'insu.gts.tif')

        new_img = np.uint8(np.logical_or(img2,img1))*255
        cv2.imwrite(save_path+'insu.gts.tif',new_img)
        img1 = cv2.imread(save_path+'cond.gtl.tif')
        img2 = cv2.imread(save_path+'cond.gts.tif')
        new_img = np.uint8(np.logical_and(img2,img1))*255
        cv2.imwrite(save_path+'cond.gts.tif',new_img)
    print(existing_layers)
    if existing_layers[-1] == ['gbl','gbs']:
        img1 = cv2.imread(save_path+'insu.gbl.tif')
        img2 = cv2.imread(save_path+'insu.gbs.tif')

        new_img = np.uint8(np.logical_or(img2,img1))*255
        cv2.imwrite(save_path+'insu.gbs.tif',new_img)
        img1 = cv2.imread(save_path+'cond.gbl.tif')
        img2 = cv2.imread(save_path+'cond.gbs.tif')
        new_img = np.uint8(np.logical_and(img2,img1))*255
        cv2.imwrite(save_path+'cond.gbs.tif',new_img)
        
    results = [insu,cond]
    return results

def zip_directory(folder_path, zip_path):
    with zipfile.ZipFile(zip_path, mode='w') as zipf:
        len_dir_path = len(folder_path)
        for root, _, files in os.walk(folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                zipf.write(file_path, file_path[len_dir_path:])

def csv2json(file):
    csvfile = open (file, newline='\n')
    spamreader = csv.reader(csvfile, delimiter=',')
    cilayers = []
    dilayers = []
    for row in spamreader:
        if row == []:
            break
        if len(row) >= 8 and row[0] != 'Number':
            if row[0] == '':
                dilayers.append(layer(row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))
            else:
                cilayers.append(layer(row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7]))
    return cilayers,dilayers

def combine_grb(grb,route,output):
    grb_txt = open(grb,'r')
    lines_grb = grb_txt.readlines()
    grb_txt.close()
    grb_route = open(route,'r')
    lines_route = grb_route.readlines()
    grb_route.close()
    scan = False
    pop = lines_grb.pop(-1)
    for row in lines_route:
        if '%ADD' in row or scan == True:
            if scan == False:
                line,value = row.split(',')
                value = '0.0010*%\n'
                line = line + ',' + value
                lines_grb.append(line)
            else:
                lines_grb.append(row)
            scan = True

    pop = lines_grb.pop(-1)
    lines_grb.append('D03*\n')
    lines_grb.append('M02*\n')
    new_name = (output+'.temp.grb').lower()
    grb_txt = open(new_name,'w')
    grb_txt.writelines(lines_grb)
    grb_txt.close()
    return new_name
    

def route2all(matching_grb,files):
    new_grb=[]
    route = [file for file in files if ".gm" in file.lower()][0]
    
    for grb in matching_grb :
        if type(grb) == list:
            new_grb.append(route2all(grb,files))
        else:
            name = str(os.path.basename(grb)).lower()
            output ='\\Code\\application_development\\Output\\Gerber_w_grb\\'+name
            new_name = combine_grb(grb,route,output)
            new_grb.append(new_name)
        
    return new_grb 

def rastrize(gerb):
    name = str(os.path.basename(gerb)).lower()
    save_file ='C:\\Code\\application_development\\Output\\tiffs\\'+name+'.cond.tiff'
    gerb = gerb.replace('/','\\')
    gerb = 'C:'+gerb+''

    subprocess.check_call([r'/WCAD/GBRIP64/gbr2tiff64.exe', str(gerb),'-tiff:outputfile','-274x','-aw','-dpi:705','-scale:1.0213,1','-inverse','-logcalls','-ram:128','-pack:'+save_file,'-units:inch','-engargs','-drawadj'])#,shell=True)
    tiff = Image.open(save_file).convert('1')
    tiff = ImageOps.crop(tiff, border=1)
    tiff = ImageOps.expand(tiff, border=1, fill=0)
    tiff.save(save_file)
#    except:
 #       msg.alert(gerb +"There was a Problem With the gerber")
    return save_file

def rastrize_list_invert(matching_grb):
    tiffs = []
    for file in matching_grb :
        if type(file) == list:
            temp_list = rastrize_list_invert(file)
            for temp in temp_list:
                tiffs.append(temp)
            
        else:
            save_file = rastrize(file)
            save_file2 = save_file.replace('.cond.','.insu.')
            tiffs.append(save_file)

        #except:
        #    msg.alert(file +" File is Not Gerber")
    return tiffs

def drl2grb(file,name,kind):
    if kind == "non_plated":
        new_grb = '\\Code\\application_development\\Output\\'+name+'.'+str(kind)+'.th.drl.grb'
    else:
        new_grb = '\\Code\\application_development\\Output\\'+name+'.'+str(kind)+'.drl.grb'
    subprocess.check_call([r'/WCAD/NETEX-G64/artwork/drl2gbr64.exe', file ,'-startdcode:200','-gbrformat:4.4','-drlformat:4.4','-gbrxymode:mm','-drlunits:mm','-gbrzerosup:leading','-out:'+new_grb,'-silence'])#,shell=True)
    return new_grb

def drl2grb_all(file,matching_grb ,diamter_del,optimized):
    txt_scan = True
    start_del = False
    data_del = []
    via_file = ['M48\n', ';Layer_Color=9474304\n', ';FILE_FORMAT=4:4\n', 'METRIC,LZ\n',]
    via_b = False
    txtfile = open(file,'r')
    index = 0
    lines = txtfile.readlines()
    txtfile.close()
    results =[]
    add = True
    name = str(os.path.basename(file)).lower()
    if diamter_del > 0.0 :
        for row in lines:
            if ";TYPE" in row : 
                temp = row.replace('\n','')  
                kind = temp.split('=')[1].lower()
            if row[0] == 'T' and txt_scan == True:
                add = True
                t_number,diameter = row.split('C')
                diameter = float(diameter.replace('\n',''))
                t_number = t_number.split('F')[0]
                if len(t_number) <= 2 :
                    t_number = t_number.replace(t_number[1],'0'+t_number[1])
                if diameter < diamter_del:
                    t_numertion = int(t_number.replace('T',''))
                    if t_numertion <= 9:
                        t_numertion = '0'+str(t_numertion+1)
                    data_del.append([t_number,'T'+t_numertion ])
                    add = False
            if txt_scan == False:
                if data_del != [] and row == data_del[0][1]+'\n':
                    add = True
                    data_del.pop(0)
                if data_del != [] and row == data_del[0][0]+'\n' or len(data_del) > 1 and row == data_del[1][0]+'\n':
                    add = False
            if row[0] == '%':
                txt_scan = False
                via_file.append(row)
            index = index + 1
            if add == True:
                results.append(row)
            else : 
                via_file.append(row)

        drill = '\\Code\\application_development\\Output\\'+name+'.'+str(kind[1])+'.org.txt'
        results.append('M30\n')
        txtfile = open(drill,'w')
        txtfile.writelines(results)
        txtfile.close()

        new_grb = drl2grb(drill,name,kind)
        via_file.append('M30\n')
        via = '\\Code\\application_development\\Output\\'+name+'.'+str(kind[1])+'.via.txt'
        save_via= open(via,'w')
        save_via.writelines(via_file)
        save_via.close()
        new_grb_via = drl2grb(via,name,'via')
        via_b = True
        if len(results) <= 6 :
            kind='via'
        if len(via_file) <= 6 :
            via_b = False
    else:
        two_kinds = False
        for row in lines:
            if ";TYPE" in row : 
                if two_kinds == False:
                    temp = row.replace('\n','')
                    kind = temp.split('=')[1]
                else:
                    msg.alert(file +"This Drill File has 2 kind of holes this feture is not \n yet suporrted please break up to multiple files ")
                    return
                two_kinds = True
            if row[0] == '%':
                break
        new_grb = drl2grb(file,name,kind)
    
    if kind == "plated" :
        
        grb_sub = '/Code/application_development/Output/'+name+'.'+str(kind)+'.drl.sub.th'
        if optimized == True :
            chang_app_size(new_grb,grb_sub)
        else:
            subprocess.check_call([r'/WCAD/GBRVU64/artwork/gbrunmgr64.exe', new_grb ,'-wdir:\\Code\\application_development\\Output\\','-out:'+grb_sub,
                '-maxpoints:4000','-arcres:9','-arcsag:0.001mm' , '-outputtype:rs274x','-sizingvalue:0.16mm ','-polyformat:butting',# '-silent',
                '-thrnum:4 ','-circularize:0.1mm', '-no_warnings'])#,'-bgn_split_args','-fmt:4.4','-end_split_args'])#,shell=True)
            subprocess.check_call([r'/wcad/GBRPREP64/gbrprep64.exe', grb_sub, '-rewrite!'+grb_sub+'.grb', 
            '-thrnum:4','-fmt:44','-arcres:45.000000','-chord_error:0.001000mm','-maxpts:8191','-maxsrxy:32768','-maxblkvrtcnt:50000000','-highest_dcode:9999'])
        sleep(1)
        matching_grb.append([new_grb,grb_sub+'.grb'])
        

    if kind == "non_plated":
        sleep(1)
        grb_th = '\Code\application_development\Output\\'+name+'.'+str(kind)+'.th.drl.grb'
        matching_grb.append(grb_th)
        

    if via_b == True:
        matching_grb.append(new_grb_via)



def prepare_prepreg(tiffs):
    prepreg = []
    drl = []
    drl_insu = []
    for pair in tiffs :
        if pair != [] :
            name = 'prepreg.'+pair[0][0]+'.'+pair[0][1]+'.tif'
            names = 'drl.sub.'+pair[0][0]+'.'+pair[0][1]+'.tif'
            named = 'drl.'+pair[0][0]+'.'+pair[0][1]+'.tif'
            save_path = './Output/tiffs/pack/'
            tiff_first1 = ''
            tiff_first = ''
            org = False
            th =False
            if len(pair[1]) > 1  :
                for i in range (0,len(pair[1])):
                    if not ".sub" in pair[1][i] and ".th" in pair[1][i]  :
                        i4 =  Image.open(pair[1][i]).convert('1')
                        i4.save(save_path+named)
                        drl.append(save_path+named)
                    if "plated" in pair[1][i] and not ".insu." in pair[1][i]:
                        i4 =  Image.open(pair[1][i]).convert('1')
                        if '.drl.sub' in pair[1][i]:
                            i4.save(save_path+names)
                            drl_insu.append(save_path+names)
                            
                            iname = pair[1][i].replace(".plated.drl.sub.th.",".plated.drl.")
                            i3 = Image.open(iname).convert('1')
                            i5=  Image.open(pair[1][i]).convert('1')
                            i2 = ImageChops.subtract( i5,i3,scale=1.0,offset=0)
                            
                        else:
                            i4.save(save_path+named)
                            drl.append(save_path+named)
                            continue
                    else :
                        if org == False :
                            tiff_first = pair[1][i]
                            i1 = Image.open(tiff_first).convert('1')
                            org =True
                        i2 = Image.open(pair[1][i]).convert('1')

                    if org == True :
                        PIL = ImageChops.add( i1,i2,scale=1.0,offset=0)

                    else:
                        org =True
                        PIL = i2

                    i1 = PIL
                
            

            if org == True :
                i1.save(save_path+name)
                prepreg.append(save_path+name)
            
#
    return prepreg,drl,drl_insu


def order(ldp,org_grb,tiffs):

    layers_ext = []
    ldp_file = open(ldp[0],'r')
    ldp_lines= ldp_file.readlines()
    ldp_file.close()
    existing_layers = []

    for grb in org_grb : 
        existing_layers.append(grb.split('.')[-1].lower())#given layers
    existing_layers = order_list(existing_layers)

    for line in ldp_lines:#planed layers

        line = line.replace('\n','')  
        kind = line.split('=')
        if len(kind) == 4:
            layers_names = kind[1].split('_')
            layers_ext.append([kind[2].split('|')[0]+'.',kind[3].split(',')])


    if len(existing_layers) > 1 :
        for file_index in range (0,len(existing_layers)-1):
            existing_layers[file_index] = [existing_layers[file_index],existing_layers[file_index+1]]
        existing_layers.pop(-1)  
        files_with_drl=[]
        for layer in existing_layers :
            combo = []
            for ext in layers_ext :
                
                if layer[0] in ext[1] or layer[0] == 'gts' and layer[1] in ext[1] or layer[1] == 'gbs':
                    
                    tiff_add = [tiff for tiff in tiffs if ext[0] in tiff]
                    for add in tiff_add:
                        combo.append(add)
                    
            files_with_drl.append([layer,combo])
        result,drl,drl_insu = prepare_prepreg(files_with_drl) 
        
        return result,drl,drl_insu,existing_layers
           
    else:# if only one layer special case not written
        ###print(kind)
        pass
    ###print(org_grb)
    ###print(layers_ext)


def chang_app_size(grb,output):
    grb_txt = open(grb,'r')
    lines = grb_txt.readlines()
    grb_txt.close()
    cheanged = False
    delta = 0.16
    for row in lines:
        ##print(row)
        if '%ADD' in row :
            cheanged = True
            if 'X' in row  :
                s_row = row.split(',')
                line = s_row[-1].split('X')
                j1 = float(line[-1].replace('*%\n',''))+delta
                j2 = float(line[0])+delta


                x1 = str(format(j1,".4f"))+'*%\n'
                x2 = str(format(j2,".4f"))+'X'
                if 'P' in row :
                    new_line = s_row[0]+','+x2+line[-2]+'X'+line[-1]
                else:
                    new_line = s_row[0]+','+x2+x1
            else:
                line = row.split(',')
                j1 = float(line[-1].replace('*%\n',''))+delta
                x1 = str(format(j1,".4f"))+'*%\n'

                new_line = line[0]+','+x1
            lines[lines.index(row)] = new_line
        elif not '%ADD'in row and cheanged == True:
            break
    #new_name = ('C:\\Code\\application_development\\gerber_test'+'.temp.grb').lower()
    grb_txt = open(output+'.grb','w')
    grb_txt.writelines(lines)
    grb_txt.close()
    return output

def order_list(existing_layers):
    def atoi(text):
        return int(text) if text.isdigit() else text
    def natural_keys(text):
        return [ atoi(c) for c in re.split('(\d+)',text) ]
    
    SORT_ORDER = {"gtp": 0, "gto": 1, "gts": 2, "gtl": 3, "gbl": 4, "gbs": 5 ,"gbp": 6, "gbo": 7}
    new_list = []
    flattened_list = []
    insert = False
    temp_list = existing_layers[:]
    for layer in temp_list:
        if layer == "gtp" : 
            new_list.append(layer)
            existing_layers.pop(existing_layers.index(layer))
        if layer == "gts" :
            new_list.append(layer)
            existing_layers.pop(existing_layers.index(layer))
        if layer == "gto" :
            new_list.append(layer)
            existing_layers.pop(existing_layers.index(layer))
        if layer == "gtl" : 
            new_list.append(layer) 
            existing_layers.pop(existing_layers.index(layer))
        if layer == "gbl" : 
            new_list.append(layer)
            existing_layers.pop(existing_layers.index(layer))    
        if layer == "gbs" : 
            new_list.append(layer)
            existing_layers.pop(existing_layers.index(layer))
        if layer == "gbo" : 
            new_list.append(layer)
            existing_layers.pop(existing_layers.index(layer))
        if layer == "gbp" : 
            new_list.append(layer)
            existing_layers.pop(existing_layers.index(layer))
    new_list.sort(key= lambda val: SORT_ORDER[val])
    existing_layers.sort(key=natural_keys)
    for layer in new_list:
        if layer[1] != 't' and insert == False:
            insert = True
            new_list.insert(new_list.index(layer) , (existing_layers))
            for ext in existing_layers:
                flattened_list.append(ext)
        else:
            flattened_list.append(layer)
    return flattened_list




if __name__ == "__main__":
    main()